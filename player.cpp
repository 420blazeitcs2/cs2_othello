#include "player.h"
#include <unistd.h>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = true;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */

    // could probably precalculate tons of shit, but that's effort.

    board = new Board();
    this->side = side;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete board;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    // Update board state
    if(opponentsMove){
        // My bet is enums are automagically interpreted as integers, and integers as bools?
        // Could probably do something with that to save negligible amounts of time.
        // Maybe the new side is !side
        board->doMove(opponentsMove, side == BLACK ? WHITE : BLACK);
    }

    // Calculate new move
    //sleep(1);

    if(!board->hasMoves(side))
        return NULL;

	int h[8][8] = {
		{99,-8,8,6,6,8,-8,99},
		{-8,-24,-4,-3,-3,-4,-24,-8},
		{8,-4,7,4,4,7,-4,8},
		{6,-3,4,0,0,4,-3,6},
		{6,-3,4,0,0,4,-3,6},
		{8,-4,7,4,4,7,-4,8},
		{-8,-24,-4,-3,-3,-4,-24,-8},
		{99,-8,8,6,6,8,-8,99}};
	int max;
	Move * currentMove = NULL;
	
	// Use the minimax
	if (testingMinimax)
	{
		bool mmaxInit = false;
		int min;
		// Compute the minimum possible score for each possible move
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				Move * temp = new Move(i, j);
				if (board->checkMove(temp, side))
				{
					min = recursiveMinimax(board, temp, 2)
						+ h[temp->getX()][temp->getY()];
					if (!mmaxInit)
					{
						max = min;
						currentMove = temp;
						mmaxInit = true;
					}
					else if (min > max)
					{
						max = min;
						delete currentMove;
						currentMove = temp;
					}
					else if (min == max)
					{
						if (h[temp->getX()][temp->getY()] >
							h[currentMove->getX()][currentMove->getY()])
						{
							delete currentMove;
							currentMove = temp;
						}
						else
						{
							delete temp;
						}
					}
					else
					{
						delete temp;
					}
				}
				else
				{
					delete temp;
				}
			}
		}
		if (currentMove)
		{
			board->doMove(currentMove, side);
			return currentMove;
		}
		return NULL;
	}	
    // Use our awesome heuristic
    max = -1000000;

    for(int x = 0; x < 8; x++){
        for(int y = 0; y < 8; y++){
            Move * temp = new Move(x, y);
            if(board->checkMove(temp, side)){
                if(heuristic(temp) > max){
                    max = heuristic(temp);
                    currentMove = temp;
                }
                else
				{
					delete temp;
				}
            }
            else
            {
				delete temp;
			}
        }
    }

    if(currentMove){
        board->doMove(currentMove, side);
        return currentMove;
    }
    return NULL;
}

// The most ingenious of heuristics.
// Corners > edges > everything else
// Good enough to beat random?
int Player::heuristic(Move* move){
   /* if((move->getX() == 0 && move->getY() == 0) ||
            (move->getX() == 0 && move->getY() == 7) ||
            (move->getX() == 7 && move->getY() == 0) ||
            (move->getX() == 7 && move->getY() == 7))
        return 20; // Corners are awesome!

    if((move->getX() == 1 && move->getY() == 0) ||
            (move->getX() == 1 && move->getY() == 7) ||
            (move->getX() == 6 && move->getY() == 0) ||
            (move->getX() == 6 && move->getY() == 7) ||
            (move->getX() == 0 && move->getY() == 1) ||
            (move->getX() == 0 && move->getY() == 7) ||
            (move->getX() == 7 && move->getY() == 1) ||
            (move->getX() == 7 && move->getY() == 6))
        return 0; // Not corners are not awesome!


    if(move->getX() == 0 || move->getX() == 7 || move->getY() == 0 || move->getY() == 7)
		return 10; // Edges are... I have no idea.

    // inefficiently figure out which moves give more points
    Board* test = board->copy();
    int curScore = test->count(side);

    test->doMove(move, side);

    int nowScore = test->count(side);

    delete test;
    return nowScore - curScore;
    */
    int h[8][8] = {
        {99,-8,8,6,6,8,-8,99},
        {-8,-24,-4,-3,-3,-4,-24,-8},
        {8,-4,7,4,4,7,-4,8},
        {6,-3,4,0,0,4,-3,6},
        {6,-3,4,0,0,4,-3,6},
        {8,-4,7,4,4,7,-4,8},
        {-8,-24,-4,-3,-3,-4,-24,-8},
        {99,-8,8,6,6,8,-8,99}
    };

    return h[move->getX()][move->getY()];
}

int Player::minimax(Move *move)
{
	Board * test = board->copy();
	int curScore = test->count(side);
	int newScore;
	int min = 0;
	bool minInit = false;
	Side other = (side == BLACK) ? WHITE : BLACK;
	
	test->doMove(move, side);
	if (test->isDone())
	{
		return test->count(side) - curScore;
	}
	
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			Move * temp = new Move(i, j);
			if (test->checkMove(temp, other))
			{
				Board * extra = test->copy();
				test->doMove(temp, other);
				newScore = test->count(side);
				if (!minInit)
				{
					min = newScore - curScore;
					minInit = true;
				}
				else if (newScore - curScore < min)
				{
					min = newScore - curScore;
				}
				delete test;
				test = extra;
			}
			delete temp;
		}
	}
	delete test;
	return min;
}

void Player::setBoard(Board * board)
{
	if (testingMinimax)
	{
		this->board = board;
	}
}

int Player::minimax(Board * tboard, Move * move)
{
	Board * test = tboard->copy();
	int curScore = test->count(side);
	int newScore;
	int min = 0;
	bool minInit = false;
	Side other = (side == BLACK) ? WHITE : BLACK;
	
	test->doMove(move, side);
	if (test->isDone())
	{
		return test->count(side) - curScore;
	}
	
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			Move * temp = new Move(i, j);
			if (test->checkMove(temp, other))
			{
				Board * extra = test->copy();
				test->doMove(temp, other);
				newScore = test->count(side);
				if (!minInit)
				{
					min = newScore - curScore;
					minInit = true;
				}
				else if (newScore - curScore < min)
				{
					min = newScore - curScore;
				}
				delete test;
				test = extra;
			}
			delete temp;
		}
	}
	delete test;
	return min;
}

int Player::recursiveMinimax(Board * tboard, Move * move, int depthLeft)
{
	int result;
	int min;
	bool minInit = false;
	Board * test = tboard->copy();
	test->doMove(move, side);
	Side other = (side == BLACK) ? WHITE : BLACK;
	if (depthLeft == 0 || test->isDone())
	{
		delete test;
		return minimax(tboard, move);
	}
	else
	{
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				Move * temp = new Move(i, j);
				if (not test->checkMove(temp, other))
				{
					delete temp;
					continue;
				}
				Board * extra = test->copy();
				extra->doMove(temp, other);
				if (extra->isDone())
				{
					delete extra;
					delete temp;
					min = minimax(tboard, move);
					minInit = true;
					continue;
				}
				for (int x = 0; x < 8; x++)
				{
					for (int y = 0; y < 8; y++)
					{
						Move * possible = new Move(x, y);
						if (not extra->checkMove(possible, side))
						{
							delete possible;
							continue;
						}
						Board * toPass = extra->copy();
						if (not minInit)
						{
							min = recursiveMinimax(toPass, possible, depthLeft - 1);
							minInit = true;
						}
						else
						{
							result = recursiveMinimax(toPass, possible, depthLeft - 1);
							if (result < min)
							{
								min = result;
							}
						}
						delete toPass;
						delete possible;
					}
				}
				delete extra;
				delete temp;
			}
		}
	}
	delete test;
	return min;
}
