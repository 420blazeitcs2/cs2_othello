#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    void setBoard(Board * board);

private:
    Board* board;
    Side side;

    int heuristic(Move* move);
    int minimax(Move* move);
    int minimax(Board * tboard, Move * move);
    int recursiveMinimax(Board * tboard, Move * move, int depthLeft);

};

#endif
