Lev implemented a basic doMove(), and wrote a function which heuristically evaluated possible moves,
which he then improved upon by weighting different points on the game's grid differently.

Nailen wrote and implemented a 2-ply minimax algorithm, and then made it recursive so that it could
examine arbitrarily deep move trees.


Our AI started as a simple heuristic which checked whether the square was a corner, adjacent to a
corner, or on an edge, which Lev weighted differently, and simply returned the AI's score after
moving to a square minus its previous score for moves that didn't satisfy these criteria. This was
good enough to beat the random player. Then, we added iterative minimaxing for one layer. Then, we
found an online source which provided a better heuristic, giving all squares some values. Finally,
we made our minimaxing recursive and combined it with the heuristic, adding the heuristic values to
the return values of our minimax to make it favor edges and corners. We think this will work fairly
well since it not only favors strategically advantageous moves such as to the corners or edges and
avoids moves to the squares adjacent to corners, but it also uses a three-layer deep recursive
minimax function to avoid moves that give the opponent the opportunity to decrease our score by
a large amount. We tried increasing the depth of our minimax function to four layers, but found that
it ran too slowly to complete games consistently in 16 minutes. We also tried several heuristics
before settling on one that we feel works best.
